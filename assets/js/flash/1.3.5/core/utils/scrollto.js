// Vanilla JavaScript Scroll to Anchor
// @ https://perishablepress.com/vanilla-javascript-scroll-anchor/
flashCore.prototype.scrollTo = function(){
	function flashScrollAnchors(e, respond) {
		var distanceToTop = function(el) {return Math.floor(el.getBoundingClientRect().top)};
		e.preventDefault();
		var targetID = (respond) ? respond.getAttribute('href') : this.getAttribute('href');
		var targetAnchor = document.querySelector(targetID);
		if (!targetAnchor) return;
		var originalTop = distanceToTop(targetAnchor);
		window.scrollBy({ top: originalTop, left: 0, behavior: 'smooth' });
		var checkIfDone = setInterval(function() {
			var atBottom = window.innerHeight + window.pageYOffset >= document.body.offsetHeight - 2;
			if (distanceToTop(targetAnchor) === 0 || atBottom) {
				targetAnchor.tabIndex = '-1';
				targetAnchor.focus();
				window.history.pushState('', '', targetID);
				clearInterval(checkIfDone);
			}
		}, 100);
	}	

	var links = document.querySelectorAll('.scroll');
	links.forEach(function(each){return (each.onclick = flashScrollAnchors)});
}