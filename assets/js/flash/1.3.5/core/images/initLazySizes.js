flashCore.prototype.initLazySizes = function(){
    // Lazysizes
    // ===========================
    function initLazySizes() {
    	if(!document.body.classList.contains('js_init--lazyload')) {
    		document.body.classList.add('js_init--lazyload');
			document.addEventListener('lazybeforeunveil', function(e){
				var target = e.target;
				// Lazy loading for background images
				var bg = target.getAttribute('data-bg');
				if(bg){
					e.target.style.backgroundImage = 'url(' + bg + ')';
				}
				// Lazy loading map
				if(target.classList.contains('block_map') && document.body.querySelector('#map')) {
					searchMap = new searchInterface();
				}
			});
			if('objectFit' in document.documentElement.style === false) {
				document.addEventListener('lazyloaded', function(e) {
					flash.objectFitFix();
				});
				if(!document.querySelectorAll('.lazyload') || document.querySelectorAll('.lazyload').length == 0) {
					flash.objectFitFix();
				}
			}
			lazySizes.init();
    	}
    }
    window.addEventListener('scroll', function(){
    	initLazySizes();
    });

    initLazySizes();
}