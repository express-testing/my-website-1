/**
 * Wait for a specific condition to happen, then execute callback
 * @param  {function} 	condition   a function that returns the condition value
 * @param  {function} 	callback   	a callback to execute if the condition is true
 * @param  {int} 		timing   	time to wait between retries
 */
flashCore.prototype.waitCondition = function(condition, callback, timing, iterations) {
	var iterations 	= 0;
	var max_iterations 	= (10 || iterations);
	var timing 		= 20;

	function flashCheckCondition() {
		if(condition()) {
			callback();
		} else if(iterations < max_iterations) {
			iterations++;
			setTimeout(flashCheckCondition, timing);
		}
	}

	flashCheckCondition();
}